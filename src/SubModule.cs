﻿using TaleWorlds.Core;
using TaleWorlds.MountAndBlade;
using TaleWorlds.CampaignSystem;
using System.Windows.Forms;
using System;
using System.Text;
using HarmonyLib;
using PartyAIOverhaulCommands.src.Behaviours;

namespace PartyAIOverhaulCommands
{
	public class SubModule : MBSubModuleBase
	{
		protected override void OnSubModuleLoad()
		{
			base.OnSubModuleLoad();
			try
			{
				//Environment.SetEnvironmentVariable("HarmonyInstance.DEBUG", "true");
				var harmony = new Harmony("mod.octavius.bannerlord");
				harmony.PatchAll();
			}
			catch (Exception e)
			{
				MessageBox.Show("Couldn't apply Harmony due to: " + Utils.FlattenException(e));
			}
		}

		protected override void OnGameStart(Game game, IGameStarter gameStarterObject)
		{
			try
			{
				base.OnGameStart(game, gameStarterObject);
				if (!(game.GameType is Campaign))
				{
					return;
				}
				AddBehaviors(gameStarterObject as CampaignGameStarter);
			}
			catch(Exception e)
			{
				MessageBox.Show(Utils.FlattenException(e));
			}
		}

		public override void OnGameInitializationFinished(Game game)
		{
			

		}
		

		private void AddBehaviors(CampaignGameStarter gameStarterObject)
		{
			if (gameStarterObject != null)
			{
				gameStarterObject.AddBehavior(PartyAICommandsBehavior.Instance);
			}
		}

	}
}