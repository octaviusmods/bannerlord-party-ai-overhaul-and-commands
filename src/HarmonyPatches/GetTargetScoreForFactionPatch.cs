﻿using HarmonyLib;
using System;
using System.Reflection;
using System.Windows.Forms;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.SandBox.GameComponents;
using TaleWorlds.CampaignSystem.SandBox.GameComponents.Map;
using TaleWorlds.Core;
using TaleWorlds.Library;

namespace PartyAIOverhaulCommands
{
    [HarmonyPatch(typeof(DefaultTargetScoreCalculatingModel), "GetTargetScoreForFaction")]
    [HarmonyPatch(new Type[] { typeof(Settlement), typeof(Army.ArmyTypes), typeof(MobileParty), typeof(float) })]
    public class GetTargetScoreForFactionPatch
    {
        static void Postfix(DefaultTargetScoreCalculatingModel __instance, Settlement targetSettlement, Army.ArmyTypes missionType, MobileParty mobileParty, float ourStrength, ref float __result)
        {
            if (mobileParty.LeaderHero == null)
                return;
            if (missionType == Army.ArmyTypes.Raider)
            {
                if (targetSettlement.OwnerClan != null)
                {
                    float relation = (float)targetSettlement.OwnerClan.Leader.GetRelation(mobileParty.LeaderHero) / 20;
                    float relation_modifier = Math.Max(Math.Min(relation, 1f), -1f);
                    __result *= 1 - (relation_modifier * (relation_modifier > 0 ? (1 - Config.Value.RelationRaidingPositiveMultMin) : (Config.Value.RelationRaidingNegativeMultMax - 1)));
                }
                if (targetSettlement.Culture == mobileParty.LeaderHero.Culture || targetSettlement.Culture == mobileParty.MapFaction.Culture)
                    __result *= Config.Value.SameCultureRaidingMult;
            }
            else if (missionType == Army.ArmyTypes.Besieger)
            {
                //if (mobileParty.Army != null && mobileParty.Army.LeaderParty == mobileParty && targetSettlement.IsFortification && __result != 0f)
                    //InformationManager.DisplayMessage(new InformationMessage(mobileParty.Name + " Settlement Score: " + __result.ToString(), Colors.Green));
                if (targetSettlement.OwnerClan != null)
                {
                    float relation = (float)targetSettlement.OwnerClan.Leader.GetRelation(mobileParty.LeaderHero) / 20;
                    float relation_modifier = Math.Max(Math.Min(relation, 1f), -1f);
                    __result *= 1 - (relation_modifier * (relation_modifier > 0 ? (1 - Config.Value.RelationSiegingPositiveMultMin) : (Config.Value.RelationSiegingNegativeMultMax - 1)));
                }
                if (targetSettlement.Culture == mobileParty.MapFaction.Culture)
                __result *= Config.Value.SameCultureSiegingMult;
            }
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }

        static bool Prepare()
        {
            return true;
        }
    }
}
