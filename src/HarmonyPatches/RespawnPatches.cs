﻿using HarmonyLib;
using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.Actions;
using TaleWorlds.Library;

namespace PartyAIOverhaulCommands
{
    [HarmonyPatch(typeof(ScatterCompanionAction), "ApplyInternal")]
    public class ScatterCompanionActionPatch
    {
        static bool Prefix(Hero companion, EndCaptivityDetail detail)
        {
            if (companion.IsActive)
            {
                //MessageBox.Show("Block respawn in city");
                return false;
            }
            return true;
        }
        // Respawns defeated clan parties
        static void Postfix(Hero companion, EndCaptivityDetail detail)
        {
            if (companion.Clan == Hero.MainHero.Clan && (companion.PartyBelongedTo == null || companion.PartyBelongedTo.Party.Owner != companion) && companion?.getOrder() != null && companion.IsAlive &&
                 companion.Clan.CommanderLimit > companion.Clan.WarPartyComponents.Count)
            {
                //MessageBox.Show("ScatterCompanionAction");
                //Careful not to respawn Caravans here as regular parties. Currently assuming caravans will never have a getOrder()
                companion.ChangeState(Hero.CharacterStates.Active);
                MobilePartyHelper.CreateNewClanMobileParty(companion, companion.Clan, out bool whatever);
                if (companion.PartyBelongedTo != null)
                {
                    PartyOrder order = companion.getOrder();
                    if (order.Behavior == AiBehavior.EscortParty && order.ScoreMinimum > 1f)
                        companion.PartyBelongedTo.SetInititave(0f, 1f, CampaignTime.YearsFromNow(100).RemainingHoursFromNow);
                    else
                        companion.PartyBelongedTo.SetInititave(order.AttackInitiative, order.AvoidInitiative, CampaignTime.YearsFromNow(100).RemainingHoursFromNow);
                }
            }
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }

    // Respawns clan parties at the spot when they're freed after battle
    [HarmonyPatch(typeof(EndCaptivityAction), "ApplyInternal")]
    public class EndCaptivityActionPatch
    {
        // Respawn Vassal Companions, usually spouse.
        static bool Prefix(Hero prisoner, EndCaptivityDetail detail)
        {
            if (prisoner.Clan == Hero.MainHero.Clan && prisoner.PartyBelongedToAsPrisoner != null && !prisoner.IsActive && prisoner.IsAlive && (prisoner.PartyBelongedTo == null || prisoner.PartyBelongedTo.Party.Owner != prisoner) &&
                (detail == EndCaptivityDetail.ReleasedAfterBattle || detail == EndCaptivityDetail.ReleasedAfterPeace || detail == EndCaptivityDetail.RemovedParty) &&
                 prisoner.Clan.CommanderLimit > prisoner.Clan.WarPartyComponents.Count &&
                 prisoner.PartyBelongedToAsPrisoner.Position2D.DistanceSquared(MobileParty.MainParty.VisualPosition2DWithoutError) < 25f)
            {
                //MessageBox.Show("EndCaptivityAction");
                // Start copy code from original method
                StatisticsDataLogHelper.AddLog(StatisticsDataLogHelper.LogAction.EndCaptivityAction);
                PartyBase partyBelongedToAsPrisoner = prisoner.PartyBelongedToAsPrisoner;
                IFaction faction;
                if (partyBelongedToAsPrisoner == null)
                {
                    IFaction neutralFaction = CampaignData.NeutralFaction;
                    faction = neutralFaction;
                }
                else
                {
                    faction = partyBelongedToAsPrisoner.MapFaction;
                }
                IFaction capturerFaction = faction;
                //CampaignEventDispatcher.Instance.OnPrisonerReleased(prisoner, capturerFaction, detail); // Proctected method
                Traverse.Create(CampaignEventDispatcher.Instance).Method("OnHeroPrisonerReleased", new Type[] { typeof(Hero), typeof(PartyBase), typeof(IFaction), typeof(EndCaptivityDetail) }).GetValue(new object[] { prisoner, partyBelongedToAsPrisoner, capturerFaction, detail });
                // End copy code from original method

                SpawnPartyAtPosition(prisoner, MobileParty.MainParty.VisualPosition2DWithoutError);
                if (prisoner.PartyBelongedTo != null && prisoner?.getOrder() != null)
                {
                    PartyOrder order = prisoner.getOrder();
                    if (order.Behavior == AiBehavior.EscortParty && order.ScoreMinimum > 1f) {
                        if (Campaign.Current.Models.MapDistanceModel.GetDistance(prisoner.PartyBelongedTo, MobileParty.MainParty) > 15f)
                            prisoner.PartyBelongedTo.SetInititave(0f, 1f, CampaignTime.YearsFromNow(100).RemainingHoursFromNow);
                        else
                            prisoner.PartyBelongedTo.SetInititave(order.AttackInitiative, order.AvoidInitiative, CampaignTime.YearsFromNow(100).RemainingHoursFromNow);
                        prisoner.PartyBelongedTo.SetMoveEscortParty(order.TargetParty);
                    }
                }
                return false;
            }
            return true;
        }

        static void SpawnPartyAtPosition(Hero hero, Vec2 position)
        {
            if (!hero.IsActive && hero.IsAlive)
            {
                hero.ChangeState(Hero.CharacterStates.Active);
                GiveGoldAction.ApplyBetweenCharacters(null, hero, 3000, true);
                //Traverse.Create(hero.Clan).Method("AddCommander", new Type[] { typeof(Hero) }).GetValue(new object[] { hero }); //Method disappeared in e1.4.3, commanders not tracked separately anymore?
                MobilePartyHelper.SpawnLordParty(hero, position, 0.5f);
            }
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }
}
