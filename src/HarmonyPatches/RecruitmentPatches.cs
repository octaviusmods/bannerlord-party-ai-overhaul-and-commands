﻿using HarmonyLib;
using Helpers;
using PartyAIOverhaulCommands.src.Behaviours;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.Actions;
using TaleWorlds.CampaignSystem.SandBox.CampaignBehaviors;
using TaleWorlds.CampaignSystem.SandBox.CampaignBehaviors.AiBehaviors;
using TaleWorlds.CampaignSystem.ViewModelCollection;
using TaleWorlds.Core;
using TaleWorlds.Library;
using Label = System.Reflection.Emit.Label;

namespace PartyAIOverhaulCommands
{

    [HarmonyPatch(typeof(RecruitPrisonersCampaignBehavior), "RecruitPrisonersAi")]
    public class RecruitPrisonersAiPatch
    {
        static bool Prefix(MobileParty mobileParty, CharacterObject troop, ref int num)
        {
            if (mobileParty?.LeaderHero?.Clan == Hero.MainHero.Clan && mobileParty.LeaderHero.getTemplate() != null)
            {
                TroopRoster template = mobileParty.LeaderHero.getTemplate();
                int template_count = template.GetTroopCount(troop);
                if (template_count == 0)
                    return false;
                else if (template_count == 1)
                    return true;
                else 
                {
                    num = template_count - mobileParty.MemberRoster.GetTroopCount(troop);
                    if (num > 1)
                        return true;
                    return false;
                }
            }
            return true;
        }


        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }

    // Is this still necessary since RecruitPrisonersAi (above) was added?
    // Stops parties exceeding troop/prisoner limits
    [HarmonyPatch(typeof(TroopRoster), "AddToCounts")]
    [HarmonyPatch(new Type[] { typeof(CharacterObject), typeof(int), typeof(bool), typeof(int), typeof(int), typeof(bool), typeof(int), })]
    public class AddToCountsPatch
    {
        static bool Prefix(TroopRoster __instance, CharacterObject character, int count, int index, ref int __result)
        {
            PartyBase partyBase = Traverse.Create(__instance).Property<PartyBase>("OwnerParty").Value;
            if (partyBase?.MobileParty?.LeaderHero != null && partyBase.MobileParty.LeaderHero != Hero.MainHero && partyBase?.MobileParty?.LeaderHero?.Clan == Hero.MainHero.Clan && partyBase.MapEvent != null && partyBase.MapEvent.HasWinner && partyBase.MemberRoster != null && partyBase.PrisonRoster != null && character != null)
            {
                //InformationManager.DisplayMessage(new InformationMessage("Runs"));
                Hero hero = partyBase.MobileParty.LeaderHero;
                if (__instance == partyBase.MemberRoster)
                {
                    if (partyBase.PartySizeLimit <= partyBase.NumberOfAllMembers)
                    {
                        __result = -1;
                        return false;
                    }
                    else if (hero.getTemplate() != null)
                    {
                        
                        TroopRoster template = hero.getTemplate();
                        int template_count = template.GetTroopCount(character);
                        if (template_count == 0)
                        {
                            //InformationManager.DisplayMessage(new InformationMessage(hero.Name + " refused freed prisoner recruit: " + character.Name));
                            __result = -1;
                        }
                        else if (template_count == 1 || (template_count - hero.PartyBelongedTo.MemberRoster.GetTroopCount(character) > 0))
                        {
                            //InformationManager.DisplayMessage(new InformationMessage(hero.Name + " recruits freed prisoner: " + character.Name));
                            return true;
                        }
                        //
                        __result = -1;
                        return false; // We should probably do swapping instead to dismiss low tier units in favor of new high tier units
                    }
                }
                else if (__instance == partyBase.PrisonRoster && (partyBase.PrisonerSizeLimit <= partyBase.NumberOfPrisoners || (hero?.getOrder()?.StopTakingPrisoners != null && hero.getOrder().StopTakingPrisoners && !character.IsHero)))
                {
                    //InformationManager.DisplayMessage(new InformationMessage("Refused prisoner"));
                    __result = -1;
                    return false; // Idem dito for prisoners
                }
            }
            return true;
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }

    [HarmonyPatch(typeof(HeroHelper), "HeroCanRecruitFromHero")]
    public class HeroCanRecruitFromHeroPatch
    {
        static bool Prefix(ref Hero buyerHero, Hero sellerHero, int index, ref bool __result)
        {

            if (buyerHero?.getTemplate() != null && buyerHero?.PartyBelongedTo?.Party != null)
            {
                if (index >= HeroHelper.MaximumIndexHeroCanRecruitFromHero(buyerHero.Clan.Leader, sellerHero, -101))
                {
                    __result = false;
                    return false;
                }
                //InformationManager.DisplayMessage(new InformationMessage(HeroHelper.MaximumIndexHeroCanRecruitFromHero(buyerHero, sellerHero, -101).ToString()));
                TroopRoster template = buyerHero.getTemplate();
                CharacterObject troop = sellerHero.VolunteerTypes[index];
                int template_count = template.GetTroopCount(troop);
                if (buyerHero.PartyBelongedTo.Party.PartySizeLimit <= buyerHero.PartyBelongedTo.Party.NumberOfAllMembers)
                {
                    __result = false;
                    return false;
                }
                if (template_count == 0)
                {
                    __result = false;
                }
                else if (template_count == 1 || (template_count - buyerHero.PartyBelongedTo.MemberRoster.GetTroopCount(troop) > 0))
                {
                    __result = true;
                    //if (__result)
                    //    InformationManager.DisplayMessage(new InformationMessage("Will take recruit: " + troop.Name.ToString() + " Gold: " + buyerHero.Gold));
                    //return true;
                }
                else
                {
                    __result = false;
                    //InformationManager.DisplayMessage(new InformationMessage("Won't take recruit: " + troop.Name.ToString()));
                }

                return false;
            }
            if (buyerHero.Clan == Clan.PlayerClan)// && buyerHero?.Spouse != CharacterObject.PlayerCharacter.HeroObject)
                buyerHero = CharacterObject.PlayerCharacter.HeroObject;
            return true;
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }


    [HarmonyPatch(typeof(AiVisitSettlementBehavior), "NumberOfVolunteersCanBeRecruitedFrom")]
    public class NumberOfVolunteersCanBeRecruitedFromPatch
    {
        static bool Prefix(MobileParty mobileParty, Settlement settlement, ref int __result)
        {
            if (mobileParty?.LeaderHero?.getTemplate() != null && mobileParty?.LeaderHero?.PartyBelongedTo?.Party != null)
            {
                int available_volunteers = 0;
                foreach (Hero notable in settlement.Notables)
                {
                    int max_index = HeroHelper.MaximumIndexHeroCanRecruitFromHero(mobileParty.LeaderHero, notable, -101);
                    for (int i = 0; i < 6; i++)
                    {
                        if (notable.VolunteerTypes[i] != null)
                        {
                            CharacterObject characterObject = notable.VolunteerTypes[i];
                            if (notable.VolunteerTypes[i] != null && i < max_index && HeroHelper.HeroCanRecruitFromHero(mobileParty.LeaderHero, notable, i))
                            {
                                available_volunteers++;
                            }
                        }
                    }
                }
                __result = available_volunteers;
                return false;
            }
            return true;
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }



    [HarmonyPatch(typeof(RecruitmentCampaignBehavior), "GetRecruitVolunteerFromMap")]
    public class GetRecruitVolunteerFromMapPatch
    {
        static bool Prefix(MobileParty side1Party)
        {
            if (side1Party?.LeaderHero?.Clan == Hero.MainHero.Clan)
                return false;
            return true;
        }


        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }

    [HarmonyPatch(typeof(RecruitmentCampaignBehavior), "ApplyRecruitMercenary")]
    public class ApplyRecruitMercenaryPatch
    {
        static bool Prefix(MobileParty side1Party, Settlement side2Party, CharacterObject subject, ref int number)
        {
            if (side1Party?.LeaderHero?.getTemplate() != null)
            {
                TroopRoster template = side1Party.LeaderHero.getTemplate();
                int template_count = template.GetTroopCount(subject);
                if (side1Party.Party.PartySizeLimit <= side1Party.Party.NumberOfAllMembers || template_count == 0)
                {
                    return false;
                }
                else if (template_count == 1)
                {
                    return true;
                } 
                else
                {
                    int needed = template_count - side1Party.MemberRoster.GetTroopCount(subject);
                    if (needed > 0)
                    {
                        number = Math.Min(number, needed);
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }



    [HarmonyPatch(typeof(PartyVM), "get_IsMainTroopsLimitWarningEnabled")]
    public class IsMainTroopsLimitWarningEnabledPatch
    {
        public static bool ignore = true;
        static bool Prefix(ref bool __result)
        {
            if (!ignore)
            {
                __result = false;
                return false;
            }
            return true;
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }



    [HarmonyPatch(typeof(PartyUpgrader), "UpgradeReadyTroops")]
    public class UpgradeReadyTroopsPatch
    {

        public static bool templateProhibitsUpgrade(PartyBase party, CharacterObject upgrade)
        { 
            if (party?.LeaderHero?.getTemplate() != null)
            {
                Hero hero = party.LeaderHero;
                TroopRoster template = hero.getTemplate();
                int template_count = template.GetTroopCount(upgrade);
                if (template_count == 0) // Check not really necessary code-wise, but may prevent a costly GetTroopCount call.
                    return true; // Not on whitelist
                else if (template_count == 1 || (template_count - hero.PartyBelongedTo.MemberRoster.GetTroopCount(upgrade) > 0))
                    return false; // No limit or limit not reached
                return true; // Limit reached
            }
            //InformationManager.DisplayMessage(new InformationMessage(party.Name + " has no Template concerning " + upgrade.Name, Colors.Green));
            return false; // Hero has no template
        }

        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions, ILGenerator il)
        {
            MethodInfo templateProhibitsUpgrade = AccessTools.Method(typeof(UpgradeReadyTroopsPatch), nameof(UpgradeReadyTroopsPatch.templateProhibitsUpgrade));
            MethodInfo get_UpgradeTargets = AccessTools.Property(typeof(CharacterObject), nameof(CharacterObject.UpgradeTargets)).GetGetMethod();
            MethodInfo get_IsBandit = AccessTools.Property(typeof(BasicCultureObject), nameof(BasicCultureObject.IsBandit)).GetGetMethod();

            var codes = new List<CodeInstruction>(instructions);
            object character_object = null;
            for (int i = 0; i < codes.Count; i++)
            {
                // Looks for:   CharacterObject characterObject = elementCopyAtIndex.Character.UpgradeTargets[j];
                if (character_object == null &&
                    codes[i].opcode == OpCodes.Stloc_S &&
                    codes[i - 1].opcode == OpCodes.Ldelem_Ref &&
                    codes[i - 2].opcode == OpCodes.Ldloc_S &&
                    codes[i - 3].opcode == OpCodes.Callvirt && codes[i - 3].operand as MethodInfo == get_UpgradeTargets)
                {
                    character_object = codes[i].operand;
                    //MessageBox.Show("CharacterObject index found! " + codes[i].operand);
                }


                if (character_object != null &&
                    codes[i].opcode == OpCodes.Stloc_S &&
                    codes[i - 1].opcode == OpCodes.Callvirt && codes[i - 1].operand as MethodInfo == get_IsBandit)
                {
                    List<Label> isBandit_labels = codes[i + 1].labels;
                    codes[i + 1].labels = null;
                    codes.Insert(i+1, new CodeInstruction(opcode: OpCodes.Ldarg_1) { labels = isBandit_labels });
                    codes.Insert(i+2, new CodeInstruction(OpCodes.Ldloc_S, character_object));
                    codes.Insert(i+3, new CodeInstruction(OpCodes.Call, templateProhibitsUpgrade));
                    Label jumpToEnd = il.DefineLabel();
                    codes.Insert(i+4, new CodeInstruction(OpCodes.Brfalse_S, jumpToEnd));
                    codes.Insert(i+5, new CodeInstruction(OpCodes.Ldc_I4_0));
                    codes.Insert(i+6, new CodeInstruction(OpCodes.Stloc_S, codes[i].operand)); //operand should be the flag
                    codes.Insert(i+7, new CodeInstruction(opcode: OpCodes.Nop) { labels = new List<Label> { jumpToEnd } });
                    return codes.AsEnumerable();
                    // Added: 
                    //if (UpgradeReadyTroopsPatch.templateProhibitsUpgrade(party, characterObject))
                    //    flag = false;
                }
            }
            MessageBox.Show("Party AI Overhaul and Commands: Failed to make AI troop upgrading adhere to recruitment templates. This is not a critical bug, you may continue playing without this feature.");
            return codes.AsEnumerable();
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }

}
