﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.Actions;
using TaleWorlds.CampaignSystem.SandBox.CampaignBehaviors.AiBehaviors;
using TaleWorlds.CampaignSystem.SandBox.GameComponents;
using TaleWorlds.CampaignSystem.SandBox.GameComponents.Map;
using TaleWorlds.Core;
using TaleWorlds.InputSystem;
using TaleWorlds.MountAndBlade;
using TaleWorlds.Library;
using SandBox;
using System.Reflection.Emit;
using TaleWorlds.CampaignSystem.CharacterDevelopment.Managers;
using TaleWorlds.CampaignSystem.SandBox.CampaignBehaviors;
using Helpers;
using TaleWorlds.SaveSystem;

namespace PartyAIOverhaulCommands
{
   


    [HarmonyPatch(typeof(SkillLevelingManager), "OnTradeProfitMade")]
    [HarmonyPatch(new Type[] { typeof(PartyBase), typeof(int) })]
    public class OnTradeProfitMadePatch
    {
        public static bool enableProfitXP = true;
        static bool Prefix()
        {
            return enableProfitXP;
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }

    [HarmonyPatch(typeof(SkillLevelingManager), "OnTradeProfitMade")]
    [HarmonyPatch(new Type[] { typeof(Hero), typeof(int) })]
    public class OnTradeProfitMade2Patch
    {
        static bool Prefix()
        {
            return OnTradeProfitMadePatch.enableProfitXP;
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }


    // Blocks vanilla troop exchange dialogue
    [HarmonyPatch]
    public class conversation_clan_member_manage_troops_on_conditionPatch
    {
        static MethodBase TargetMethod()
        {
            return AccessTools.Method( AccessTools.TypeByName("LordConversationsCampaignBehavior"),
                        "conversation_clan_member_manage_troops_on_condition",
                new Type[] { }
            );
        }
        static bool Prefix(ref bool __result)
        {
            __result = false;
            return false;
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }


    [HarmonyPatch(typeof(PartiesBuyHorseCampaignBehavior), "OnSettlementEntered")]
    public class PartiesBuyHorseCampaignBehaviorOnSettlementEnteredPatch
    {
        static bool Prefix(PartiesBuyHorseCampaignBehavior __instance, MobileParty mobileParty, Settlement settlement, Hero hero)
        {
            // Most of this code is copied from original method, with some modifications of mine. It's a mess!
            if (mobileParty != null && !mobileParty.MapFaction.IsAtWarWith(settlement.MapFaction) && mobileParty != MobileParty.MainParty && mobileParty.IsLordParty && mobileParty.LeaderHero != null && !mobileParty.IsDisbanding && settlement.IsTown)
            {
                float currentTime = Campaign.CurrentTime;
                int budget = Math.Min(100000, mobileParty.Leader.HeroObject.Gold);
                int numberOfMounts = mobileParty.Party.NumberOfMounts;
                if (numberOfMounts > mobileParty.Party.NumberOfRegularMembers)
                {
                    return false;
                }
                Town component = settlement.GetComponent<Town>();
                if (component.MarketData.GetItemCountOfCategory(DefaultItemCategories.Horse) == 0)
                {
                    return false;
                }
                float averageHorseValue = DefaultItemCategories.Horse.AverageValue;
                float ourHorsesValueToBudgetRatio = averageHorseValue * (float)numberOfMounts / (float)budget;
                if (ourHorsesValueToBudgetRatio < 0.08f)
                {
                    float randomFloat = MBRandom.RandomFloat;
                    float randomFloat2 = MBRandom.RandomFloat;
                    float randomFloat3 = MBRandom.RandomFloat;
                    float num3 = (0.08f - ourHorsesValueToBudgetRatio) * (float)budget * randomFloat * randomFloat2 * randomFloat3;
                    if (num3 > (float)(mobileParty.Party.NumberOfRegularMembers - numberOfMounts) * averageHorseValue)
                    {
                        num3 = (float)(mobileParty.Party.NumberOfRegularMembers - numberOfMounts) * averageHorseValue;
                    }
                    Traverse.Create(__instance).Method("BuyHorses", new Type[] { typeof(MobileParty), typeof(Town), typeof(float) }).GetValue(mobileParty, component, num3);
                }
            }
            if (mobileParty != null && mobileParty != MobileParty.MainParty && mobileParty.IsLordParty && mobileParty.LeaderHero != null && !mobileParty.IsDisbanding && settlement.IsTown)
            {
                float totalValueOfOurHorses = 0f;
                for (int i = mobileParty.ItemRoster.Count - 1; i >= 0; i--)
                {
                    ItemRosterElement subject = mobileParty.ItemRoster[i];
                    if (subject.EquipmentElement.Item.IsMountable)
                    {
                        totalValueOfOurHorses += (float)(subject.Amount * subject.EquipmentElement.Item.Value);
                    }
                    else if (!subject.EquipmentElement.Item.IsFood)
                    {
                        SellItemsAction.Apply(mobileParty.Party, settlement.Party, subject, subject.Amount, settlement); //Sells all non-horses and non-food
                    }
                }
                float budget = Math.Min(100000f, (float)mobileParty.LeaderHero.Gold);
                float amount_horses_to_keep = mobileParty.Party.PartySizeLimit - mobileParty.Party.NumberOfMenWithHorse; // Spare horses for potential extra infantry
                if (totalValueOfOurHorses > budget * 0.1f)
                {
                    for (int j = (int)(mobileParty.Party.NumberOfMounts - amount_horses_to_keep); j < 10; j++)
                    {
                        ItemRosterElement mostExpensiveHorse = default(ItemRosterElement);
                        int mostExpensiveHorseValue = 0;
                        foreach (ItemRosterElement itemRosterElement in mobileParty.ItemRoster)
                        {
                            if (itemRosterElement.EquipmentElement.Item.IsMountable && itemRosterElement.EquipmentElement.Item.Value > mostExpensiveHorseValue)
                            {
                                mostExpensiveHorseValue = itemRosterElement.EquipmentElement.Item.Value;
                                mostExpensiveHorse = itemRosterElement;
                            }
                        }
                        if (mostExpensiveHorseValue <= 0)
                        {
                            break;
                        }
                        SellItemsAction.Apply(mobileParty.Party, settlement.Party, mostExpensiveHorse, 1, settlement);
                        totalValueOfOurHorses -= (float)mostExpensiveHorseValue;
                        if (totalValueOfOurHorses < budget * 0.1f)
                        {
                            break;
                        }
                    }
                }
            return false;
            }
        return true;
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }

    [HarmonyPatch(typeof(PlayerTrackCompanionBehavior), "AddHeroToScatteredCompanions")]
    public class PlayerTrackCompanionBehaviorPatch
    {
        static bool Prefix(Hero hero)
        {
            if (hero.PartyBelongedTo != null && hero.getOrder() != null)
                return false;
            return true;
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }

    [HarmonyPatch(typeof(MBSaveLoad), "CheckModules")]
    public class CheckModulesPatch
    {
        public static bool missing_modules = false;
        public static MetaData meta_data;
        static void Postfix(MetaData fileMetaData, List<ModuleCheckResult> __result)
        {
            meta_data = fileMetaData;
            if (__result.Count > 0)
                missing_modules = true;
            else
                missing_modules = false;
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }

    //[HarmonyPatch(typeof(ArrangementOrder), "OnApply")]
    public class Formation_set_ArrangementOrder_Patch
    {
        static void Postfix(ArrangementOrder __instance, Formation formation)
        {
            if (__instance.OrderEnum == ArrangementOrder.ArrangementOrderEnum.ShieldWall)
            {
                int shieldbearers = formation.GetCountOfUnitsWithCondition((Agent agent) => agent.Equipment.HasShield());
                MessageBox.Show("Shieldbearers: " + shieldbearers);
                IFormationArrangement arrangement = Traverse.Create(formation).Property<IFormationArrangement>("arrangement").Value;
                int FileCount = Traverse.Create(arrangement).Property<int>("FileCount").Value;
                int RankCount = arrangement.RankCount;
                MBList2D<IFormationUnit> units2D = Traverse.Create(arrangement).Field("_units2D").GetValue< MBList2D<IFormationUnit>>();
                for (int i = 0; i < FileCount; i++)
                {
                    Agent agent = (Agent)(units2D[i, 0]);
                    if (agent.Equipment.HasShield())
                    {

                    }
                }
            }
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }

    [HarmonyPatch(typeof(RemoveCompanionAction), "ApplyInternal")]
    public class RemoveCompanionActionPatch
    {
        static void Postfix(Clan clan, Hero companion)
        {
            if (clan == Clan.PlayerClan)
                companion.cancelOrder();
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }



#if DEBUG

    //[HarmonyPatch(typeof(MobileParty), "get_SeeingRange")]
    public class get_SeeingRangePatch
    {
        static bool Prefix(MobileParty __instance, ref float __result)
        {
            if (__instance == MobileParty.MainParty)
            {
                __result = 200f;
                return false;
            }
            return true;
        }

        static void Finalizer(Exception __exception)
        {
            if (__exception != null)
            {
                MessageBox.Show(__exception.FlattenException());
            }
        }
    }
#endif

}
